CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module allows the user to add help message in a config form.
It is based on the philosophy of the field_group module.


REQUIREMENTS
------------
This module requires the following modules:

 * field_group (https://www.drupal.org/project/field_group)


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

CONFIGURATION
-------------
-- Create help text --
You can configure several help widget in your form.
    1. Go into a form display configuration page
    2. Click on the "Add Group" provided by the field_group module.
    3. Select the "Helper group" type.
    3. Configure your help text with title and description.
    4. Place the group you created wherever you want in the form.
       Be carefull, it is a group of field, so as the default
       logic of the field_group module, if the group does not
       have any children, it will not be displayed.
    5. Update or delete the field as you normally do with any field group.

-- Optional --
If you want to customize the rendering of the help text, you
can also override the template form-helper-widget.html.twig in your theme.


MAINTAINERS
-----------
tsecher - https://www.drupal.org/u/tsecher
