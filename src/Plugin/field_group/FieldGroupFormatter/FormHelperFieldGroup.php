<?php

namespace Drupal\form_helper_widget\Plugin\field_group\FieldGroupFormatter;

use Drupal\Component\Utility\Html;
use Drupal\field_group\FieldGroupFormatterBase;

/**
 * Form Helper group element.
 *
 * @FieldGroupFormatter(
 *   id = "form_helper_field_group",
 *   label = @Translation("Helper group"),
 *   description = @Translation("Wrap fields and add description"),
 *   supported_contexts = {
 *     "form",
 *     "view"
 *   }
 * )
 */
class FormHelperFieldGroup extends FieldGroupFormatterBase {

  /**
   * Field title.
   *
   * @const string
   */
  const FIELD_TITLE = 'title';

  /**
   * Field description.
   *
   * @const string
   */
  const FIELD_DESCRIPTION = 'description';

  /**
   * Default format cache.
   *
   * @var string
   */
  protected static string $defaultTextFormat;

  /**
   * REturn the current language code.
   *
   * @return string
   *   The current language id.
   */
  protected static function getCurrentLanguage(): string {
    return \Drupal::languageManager()->getCurrentLanguage()->getId();
  }

  /**
   * Return the language list.
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   *   THe language list.
   */
  protected static function getLanguages(): array {
    return \Drupal::languageManager()->getLanguages();
  }

  /**
   * {@inheritdoc}
   */
  public function process(&$element, $processed_object) {
    $settings = $this->getLocalSettings();
    $element += [
      '#type' => 'container',
      '#attributes' => [],
      0 => [
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#markup' => $settings[static::FIELD_TITLE] ?? '',
      ],
      1 => [
        '#type' => 'processed_text',
        '#text' => isset($settings[static::FIELD_DESCRIPTION]) ? $settings[static::FIELD_DESCRIPTION]['value'] : '',
        '#format' => isset($settings[static::FIELD_DESCRIPTION]) ? $settings[static::FIELD_DESCRIPTION]['format'] : '',
      ],
    ];

    // When a fieldset has a description, an id is required.
    if ($this->getSetting('description') && !$this->getSetting('id')) {
      $element['#id'] = Html::getUniqueId($this->group->group_name);
    }

    if ($this->getSetting('id')) {
      $element['#id'] = Html::getUniqueId($this->getSetting('id'));
    }

    $classes = $this->getClasses();
    if (!empty($classes)) {
      $element['#attributes'] += ['class' => $classes];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$element, $rendering_object) {
    parent::preRender($element, $rendering_object);
    $this->process($element, $rendering_object);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $form = parent::settingsForm();

    // Init all language settings, and hide to keep them in conf.
    foreach (static::getLanguages() as $language) {
      $settings = $this->getSetting($language->getId());
      $form[$language->getId()] = [
        '#type' => 'container',
        '#title' => $language->getId(),
        '#tree' => TRUE,
        static::FIELD_TITLE => [
          '#type' => 'hidden',
          '#default_value' => $settings[static::FIELD_TITLE] ?? '',
        ],
        static::FIELD_DESCRIPTION => [
          '#type' => 'container',
          '#tree' => TRUE,
          'value' => [
            '#type' => 'hidden',
            '#default_value' => $settings[static::FIELD_DESCRIPTION]['value'] ?? '',
          ],
          'format' => [
            '#type' => 'hidden',
            '#default_value' => $settings[static::FIELD_DESCRIPTION]['format'] ?? '',
          ],
        ],
      ];
    }

    $lang = static::getCurrentLanguage();
    $settings = $this->getLocalSettings();
    $form[$lang][static::FIELD_TITLE] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $settings[static::FIELD_TITLE] ?? '',
      '#required' => TRUE,
    ];

    $description = $settings[static::FIELD_DESCRIPTION];
    $form[$lang][static::FIELD_DESCRIPTION] = [
      '#type' => 'text_format',
      '#format' => array_key_exists('format', $description) ? $description['format'] : 'full_html',
      '#title' => $this->t('Description'),
      '#default_value' => array_key_exists('value', $description) ? $description['value'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getLocalSettings();
    $summary = [];
    $summary[] = $settings[static::FIELD_TITLE] ?? '';

    return $summary;
  }

  /**
   * Return the localized settings.
   *
   * @return array
   *   The local settings
   */
  public function getLocalSettings(): array {
    $lang = static::getCurrentLanguage();
    $languaged_settings = $this->getSetting($lang);

    // If languaged settings exists.
    if (!empty($languaged_settings[static::FIELD_TITLE])) {
      return $languaged_settings;
    }

    // If cascade translation exists.
    $languages_list = static::getLanguages();
    $languages_codes = array_keys($languages_list);
    $languages_codes = array_slice($languages_codes, 0, array_search($lang, $languages_codes));
    $languages_codes = array_reverse($languages_codes);

    // Foreach language.
    foreach ($languages_codes as $lang_code) {
      $languaged_settings = $this->getSetting($lang_code);
      if (!empty($languaged_settings[static::FIELD_TITLE])) {
        return $languaged_settings;
      }
    }

    // If untranslated element exists.
    $languaged_settings = $this->getSettings();
    if (!empty($languaged_settings[static::FIELD_TITLE])) {
      return $languaged_settings;
    }

    return [
      static::FIELD_TITLE => '',
      static::FIELD_DESCRIPTION => [
        'value' => '',
        'format' => static::getDefaultFormatText(),
      ],
    ];
  }

  /**
   * Return the default text format id.
   *
   * @return string
   *   The default text format id.
   */
  public static function getDefaultFormatText(): string {
    if (!isset(static::$defaultTextFormat)) {
      static::$defaultTextFormat = '';
      if ($formats = \Drupal::entityTypeManager()->getStorage('filter_format')) {
        static::$defaultTextFormat = array_keys($formats->loadMultiple())[0];
      }
    }
    return static::$defaultTextFormat;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultContextSettings($context) {
    $defaults = [
      'required_fields' => $context == 'form',
    ] + parent::defaultContextSettings($context);

    foreach (\Drupal::languageManager()->getLanguages() as $language) {
      $defaults[$language->getId()] = [
        static::FIELD_TITLE => '',
        static::FIELD_DESCRIPTION => [
          'format' => 'full_html',
          'value' => '',
        ],
      ];
    }

    return $defaults;
  }

}
